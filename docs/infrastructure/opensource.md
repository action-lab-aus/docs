# Open Sourcing

Our standard policy is that source code should be released as `MIT licensed`, and that print material and documentation be released as `Creative Commons BY`.

In practice you should create and check your license before putting any source code online.