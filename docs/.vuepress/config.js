module.exports = {
    title: 'Documentation',
    dest:'public',
    themeConfig: {
        logo: '/logo.svg',
        repo: 'https://gitlab.com/action-lab-aus/docs',
        lastUpdated: 'Updated',
        docsDir: 'docs',
        editLinks: true,
        editLinkText: 'Edit in GitLab',
        repoLabel: 'Update Docs',
        sidebar: {
            '/lab/':[
                '/lab/'
            ],
            '/policy/':[
                '/policy/',
                '/policy/rdm'
            ],
            '/infrastructure/':[
                '/infrastructure/',
                '/infrastructure/hosting',
                '/infrastructure/opensource',
                '/infrastructure/apps'
            ]
        },
        nav: [
            { text: 'Lab', link: '/lab/' },
            { text: 'Policy', link: '/policy/' },
            { text: 'Infrastructure', link: '/infrastructure/' }
          ],
    },
    head: [
        ['link', { rel: 'icon', href: '/favicon.png' }]
      ],
}