# Lab

Our lab is located in Caulfield Campus, Building H, Floor 7, Room 7.09.

## Display Screens

Follow the instructions on the screens. Use the AirTame application [https://airtame.com/start/](https://airtame.com/start/) to get connected.

## Equipment Loan

Equipment is available to loan, just get in touch with Tom.

## Branding

Is accessible at `Google Drive`/`Team Drive`/`ACTION Lab`